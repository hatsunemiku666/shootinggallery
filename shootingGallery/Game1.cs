﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Runtime;
using System;

namespace shootingGallery
{

    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D target_Sprite;
        Texture2D crosshairs_Sprite;
        Texture2D background_Sprite;
        SpriteFont gameFont;

        Vector2 targetPosition = new Vector2(300, 300);
        const int TARGET_RADIUS = 45;

        MouseState mState;
        int score = 0;
        bool mReleased = true;
        float mouseTargetDist;
        float timer = 20f;
        bool gameOver = false;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }


        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            target_Sprite = Content.Load<Texture2D>("target");
            crosshairs_Sprite = Content.Load<Texture2D>("crosshairs");
            background_Sprite = Content.Load<Texture2D>("sky");
            gameFont = Content.Load<SpriteFont>("galleryFont");
        }


        protected override void UnloadContent()
        {
          
        }

        
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            

            mState = Mouse.GetState();
            
            mouseTargetDist = Vector2.Distance(targetPosition, new Vector2(mState.X, mState.Y));
            if (timer > 0)
            {
                timer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (mState.LeftButton == ButtonState.Pressed && mReleased == true)
                {
                    if (mouseTargetDist < TARGET_RADIUS)
                    {
                        score++;
                        Random rand = new Random();

                        targetPosition.X = rand.Next(50, (graphics.PreferredBackBufferWidth - TARGET_RADIUS));
                        targetPosition.Y = rand.Next(50, (graphics.PreferredBackBufferHeight - TARGET_RADIUS));
                    }
                    else
                    {
                        score--;
                        Random rand = new Random();

                        targetPosition.X = rand.Next(50, (graphics.PreferredBackBufferWidth - TARGET_RADIUS));
                        targetPosition.Y = rand.Next(50, (graphics.PreferredBackBufferHeight - TARGET_RADIUS));
                    }
                    mReleased = false;
                }
                if (mState.LeftButton == ButtonState.Released)
                {
                    mReleased = true;
                }
            }
            else
            {
                gameOver = true;
            }
            base.Update(gameTime);
        }

       
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            spriteBatch.Draw(background_Sprite, new Vector2(0, 0), Color.White);

            if (!gameOver)
            {
                spriteBatch.Draw(target_Sprite, new Vector2(targetPosition.X - TARGET_RADIUS, targetPosition.Y - TARGET_RADIUS), Color.White);
                spriteBatch.DrawString(gameFont, "Score: " + score.ToString(), new Vector2(0, 0), Color.Black);
                spriteBatch.DrawString(gameFont, "Time: " + Math.Ceiling(timer).ToString(), new Vector2(0, 25), Color.Black);
                spriteBatch.Draw(crosshairs_Sprite, new Vector2((mState.X - 25), (mState.Y - 25)), Color.White);
            }
            else
            {
                spriteBatch.DrawString(gameFont, "Game Over!", new Vector2(graphics.PreferredBackBufferWidth / 3, graphics.PreferredBackBufferHeight / 3), Color.Black);
                spriteBatch.DrawString(gameFont, "Score: " + score.ToString(), new Vector2(graphics.PreferredBackBufferWidth / 3, (graphics.PreferredBackBufferHeight / 3) + 35), Color.Black);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
